#include <iostream>
#include <iomanip>

using namespace std;

void brick_info(float&, float&, float&, int&);
float aConvert(float&);
float SurfaceArea(float, float, float);
float Price_cal(float);
float mem_discount(float);
float mem_ch_dis(float);



int main(){
	
	cout << "* * * Ken's Brick Shop Receipt  * * *" << endl;
	cout << "* * * * * * * * * * * * * * * * * * *" << endl << endl;
	
	int total_brick;
	
	cout << "Total brick(s) : ";
	cin >> total_brick;
	
	cout << "=====================================" << endl;
	
	float length, width, height, base_price, brick_price, min, sub_total, set_price ;
	int material, membership ;
	
	
	
	
	for(int i = 0; i < total_brick; i++){
		
		cout << endl << "xxxxxxxxxxxxxx " << "#" << i+1 << " brick" << " xxxxxxxxxxxxx" << endl;
		
		//cout << "#" << i+1 << " brick ;" <<endl;
		 
		brick_info(length, width, height, material);
		
		
		
		//cout << endl << "xxxxxxxxxxxxxxxxxxxxxxxxxxxxx" << endl;
		
		aConvert(length);
		aConvert(width);
		aConvert(height);
		
		base_price = Price_cal(SurfaceArea(length, width, height));
		/*cout << endl << "= Base price is " << base_price << " THB" << endl;*/
		
		if(material == 2){
			brick_price = base_price*300;
		}else if(material == 3){
			brick_price = base_price*900;
		}else if(material == 1){
			brick_price = base_price;
		}
		
		cout << endl <<"= This brick cost " << brick_price << " THB" << endl;
		
		cout << endl <<"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" << endl;
		
		
		sub_total = brick_price;
		
		if( i == 0 ){
			min = sub_total;
		}
		if( min > sub_total){
			min = sub_total;
		}                                                                                       
		
		set_price = set_price + sub_total;  
		
		cout << "min : " << min <<endl;
		cout << "total brick(s) cost : " << set_price << endl;
		                                                    
	
	}
		
	if(total_brick == 3){
		
		cout << "-------------------------------------" << endl;
		cout << "Membership [1 = member | 0 = non-member] : " ;
		cin >> membership;
		
		cout << "....................................." << endl;
		
		if(membership==1){
			
			
			cout << "Full price = " << set_price << endl; 
			cout << "Member price = " << set_price - mem_ch_dis(min) << endl;
			
			
		}else{
			cout << "Full price = " << set_price << endl; 
			
		}		
	}else{
		
		cout << "-------------------------------------" << endl;
		cout << "Membership [1 = member | 0 = non-member] : " ;
		cin >> membership;
		
		cout << "....................................." << endl;
		
		if(membership==1){
			
			
			cout << "Full price = " << set_price << endl; 
			cout << "Member price = " << set_price - mem_discount(set_price) << endl;
			
			
		}else{
			cout << "Full price = " << set_price << endl; 
			
		}		
		
		
		
	}
	
	
	
	
	
	
	return 0;
}


void brick_info(float &length, float &width, float &height, int &material){
	
	cout << endl << "Length : ";
	cin >> length;
	
	cout << endl << "Width : ";
	cin >> width;
	
	cout << endl << "Height : ";
	cin >> height;
	
	cout << endl << "Material [1 = Clay | 2 = Gold | 3 = Unobtainium] : ";
	cin >> material;
	
}


float aConvert(float &inch_u){
	return inch_u * 2.54;
}

float SurfaceArea(float length, float width, float height){
	return ((length*width)*2) + ((length*height)*2) + ((width*height)*2);
}

float Price_cal(float Area){
	return Area*10;
}

float mem_discount(float mem_dis){
	return mem_dis*0.1;
}

float mem_ch_dis(float ch_dis){
	return ch_dis*0.2;
}

float deliv_cal(float distance, float fee){
	
	if(distance < 5){
		fee == 0;
	}else if(distance >= 5||distance <= 20){
		
		
	}
	
}

